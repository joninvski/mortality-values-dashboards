(ns pt-input
  (:require [oz.server :as server]
            [oz.core :as oz]
            [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [java-time.api :as jt]
            [clojure.pprint :as pp])
  (:import [java.time LocalDate]))

(def filepath "./data/input/PRTstmf.csv")

(def raw-csv (with-open [reader (io/reader filepath)]
               (doall
                (csv/read-csv reader))))

(def header
  (nth raw-csv 0))

(def data-rows
  (nthrest raw-csv 1))

(def raw-data
  (map
   #(zipmap header %)
   data-rows))

(def data
  (map
   (fn [row]
     (let [week-of-year (Integer. (get row "Week"))
           year (Integer. (get row "Year"))
           day-of-year (- (* week-of-year 7) 6)
           local-date (LocalDate/ofYearDay year day-of-year)]
       (assoc row :date (str local-date))))
   raw-data))

(def data (filter #(and (= (get % "Sex") "b")
                        (not= (get % "Age") "TOT")
                        (not= (get % "Age") "UNK"))
                  data))

(defn deaths-continuous [age-lower age-higher]
  {:data {:values data}
   :title (str "PRT  - Week by Week total Death")
   :height 400
   :width 1000
   :transform [{:filter {:field "Sex", :equal "b"}}
               {:filter {:field "Age", :gte age-lower}}
               {:filter {:field "Age", :lte age-higher}}]
   :layer [{:encoding {:x {:field "date" :type "temporal"
                           :timeUnit {:unit "yearmonth" :step 1}
                           :scale {:nice "month"}
                           :axis {:format "%b %Y" :labelAngle 90}}
                       :y {:field "Deaths" :aggregate "sum" :type "quantitative"}
                       :color {:field "Age" :type "nominal" :scale {:scheme "category20b"}}}
            :mark {:type "area" :tooltip true}}]})

(def viz
  [:div
   [:h1 "0 to 40"]
   [:div {:style {:display "flex" :flex-direction "row"}}
    [:vega-lite (deaths-continuous 0 40)]]
   [:h1 "40 to inf"]
   [:div {:style {:display "flex" :flex-direction "row"}}
    [:vega-lite (deaths-continuous 40 900)]]

   (for [age (range 0 90 5)]
     [:div
      [:h1 (str age " to " (+ age 5))]
      [:div {:style {:display "flex" :flex-direction "row"}}
       [:vega-lite (deaths-continuous age (inc age))]]])])

(oz/view! viz)
