(ns user
  (:require [oz.server :as server]
            [oz.core :as oz]
            [clojure.data.json :as json]
            [clojure.data.csv :as csv]
            [clojure.java.io :as io]
            [java-time.api :as jt]
            [clojure.pprint :as pp])
  (:import [java.time LocalDate]))

; (def filepath "./data/small.csv")
(def filepath "./data/stmf_2022_12_19.csv")

(do
  (def raw-csv (with-open [reader (io/reader filepath)]
                 (doall
                  (csv/read-csv reader))))

  (def header
    (nth raw-csv 2))

  (def data-rows
    (nthrest raw-csv 3))

  (def raw-data
    (map
     #(zipmap header %)
     data-rows))

  (def data
    (map
     (fn [row]
       (let [week-of-year (Integer. (get row "Week"))
             year (Integer. (get row "Year"))
             day-of-year (- (* week-of-year 7) 6)
             local-date (LocalDate/ofYearDay year day-of-year)]
         (assoc row :date (str local-date))))
     raw-data))

  (def countries
    (distinct (map #(get % "CountryCode") data)))

  (def data-by-country
    (group-by #(get % "CountryCode") data))

  (defn deaths-continuous [country-code]
    (let [country-data (get data-by-country country-code)]
      {:data {:values country-data}
       :title (str country-code " - Week by Week total Death")
       :height 400
       :width 1000
       :transform [{:filter {:field "Sex", :equal "b"}}]
       :layer [{:encoding {:x {:field "date" :type "temporal"}
                           :y {:field "DTotal" :type "quantitative"}}
                :mark {:type "point" :tooltip {:content "Year"}}
                :size {:field "DTotal" :type "quantitative"}}
               {:encoding {:x {:field "date" :type "temporal"}
                           :y {:field "DTotal" :type "quantitative"}}
                :mark {:type "line"}
                :size {:field "DTotal" :type "quantitative"}}]}))

  (defn year-deaths [country-code]
    (let [country-data (get data-by-country country-code)
          last-year-data (filter #(= (get % "Year") "2022") country-data)
          max-week (apply max (map #(Integer. (get % "Week")) last-year-data))]
      {:data {:values country-data}
       :title (str country-code " - Death by year until week " max-week)
       :height 400
       :width 1000
       :transform [{:filter {:field "Sex", :equal "b"}}
                   {:filter {:field "Week", :lte max-week}}]
       :layer [{:encoding {:x {:field "date" :type "temporal"
                               :timeUnit {:unit "year" :step 1}}
                           :y {:aggregate "sum"
                               :field "DTotal" :type "quantitative"}}
                :mark {:type "bar"}}]}))

  (defn year-deaths-rate [country-code]
    (let [country-data (get data-by-country country-code)
          last-year-data (filter #(= (get % "Year") "2022") country-data)
          max-week (apply max (map #(Integer. (get % "Week")) last-year-data))]
      {:data {:values country-data}
       :title (str country-code " - Death rate by year until week " max-week)
       :height 400
       :width 1000
       :transform [{:filter {:field "Sex", :equal "b"}}
                   {:filter {:field "Week", :lte max-week}}]
       :layer [{:encoding {:x {:field "date" :type "temporal"
                               :timeUnit {:unit "year" :step 1}}
                           :y {:aggregate "mean"
                               :field "RTotal" :type "quantitative"}}
                :mark {:type "bar"}}]}))

  (defn year-deaths-by-age [country-code]
    (let [country-data (get data-by-country country-code)
          last-year-data (filter #(= (get % "Year") "2022") country-data)
          max-week (apply max (map #(Integer. (get % "Week")) last-year-data))]
      {:data {:values country-data}
       :title (str country-code " - Death by year by age - Max week " max-week)
       :height 400
       :width 1000
       :transform [{:filter {:field "Sex", :equal "b"}}
                   {:filter {:field "Week", :lte max-week}}]
       :layer [{:encoding {:x {:field "date" :type "temporal"
                               :timeUnit {:unit "year" :step 1}}
                           :y {:aggregate "sum"
                               :field "D0_14" :type "quantitative"}}
                :mark {:type "point" :color "pink"}}
               {:encoding {:x {:field "date" :type "temporal"
                               :timeUnit {:unit "year" :step 1}}
                           :y {:aggregate "sum"
                               :field "D15_64" :type "quantitative"}}
                :mark {:type "point"}}
               {:encoding {:x {:field "date" :type "temporal"
                               :timeUnit {:unit "year" :step 1}}
                           :y {:aggregate "sum"
                               :field "D65_74" :type "quantitative"}}
                :mark {:type "point" :color "blue"}}
               {:encoding {:x {:field "date" :type "temporal"
                               :timeUnit {:unit "year" :step 1}}
                           :y {:aggregate "sum"
                               :field "D75_84" :type "quantitative"}}
                :mark {:type "point" :color "red"}}
               {:encoding {:x {:field "date" :type "temporal"
                               :timeUnit {:unit "year" :step 1}}
                           :y {:aggregate "sum"
                               :field "D85p" :type "quantitative"}}
                :mark {:type "point" :color "yellow"}}]}))

  (defn year-deaths-rate-by-age [country-code]
    (let [country-data (get data-by-country country-code)
          last-year-data (filter #(= (get % "Year") "2022") country-data)
          max-week (apply max (map #(Integer. (get % "Week")) last-year-data))]
      {:data {:values country-data}
       :title (str country-code " - Yearly Death rate by age - Max week " max-week)
       :height 400
       :width 1000
       :transform [{:filter {:field "Sex", :equal "b"}}
                   {:filter {:field "Week", :lte max-week}}]
       :layer [{:encoding {:x {:field "date" :type "temporal"
                               :timeUnit {:unit "year" :step 1}}
                           :y {:aggregate "mean"
                               :field "R0_14" :type "quantitative"}}
                :mark {:type "point" :color "pink"}}
               {:encoding {:x {:field "date" :type "temporal"
                               :timeUnit {:unit "year" :step 1}}
                           :y {:aggregate "mean"
                               :field "R15_64" :type "quantitative"}}
                :mark {:type "point"}}
               {:encoding {:x {:field "date" :type "temporal"
                               :timeUnit {:unit "year" :step 1}}
                           :y {:aggregate "mean"
                               :field "R65_74" :type "quantitative"}}
                :mark {:type "point" :color "blue"}}
               {:encoding {:x {:field "date" :type "temporal"
                               :timeUnit {:unit "year" :step 1}}
                           :y {:aggregate "mean"
                               :field "R75_84" :type "quantitative"}}
                :mark {:type "point" :color "red"}}
               {:encoding {:x {:field "date" :type "temporal"
                               :timeUnit {:unit "year" :step 1}}
                           :y {:aggregate "mean"
                               :field "R85p" :type "quantitative"}}
                :mark {:type "point" :color "yellow"}}]}))

  (def viz
    (for [country-key ["PRT" "ESP" "NOR" "SWE" "DNK" "BEL" "FRATNP" "ISL" "ITA"]]
      [:div
       [:h1 country-key]
       [:div {:style {:display "flex" :flex-direction "row"}}
        [:vega-lite (deaths-continuous country-key)]]
       [:div {:style {:display "flex" :flex-direction "row"}}
        [:vega-lite (year-deaths country-key)]]
       [:div {:style {:display "flex" :flex-direction "row"}}
        [:vega-lite (year-deaths-rate country-key)]]
       [:div {:style {:display "flex" :flex-direction "row"}}
        [:vega-lite (year-deaths-by-age country-key)]]
       [:div {:style {:display "flex" :flex-direction "row"}}
        [:vega-lite (year-deaths-rate-by-age country-key)]]]))

  (oz/view! viz))

; (oz/publish! viz)
